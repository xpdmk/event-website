<?php namespace AppBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * 
 */
class EventTranslation {
	
	use ORMBehaviors\Translatable\Translation;
	
    /**
     * @ORM\Column(type="string")
     * @Assert\NotEmpty
     * @var string
     */
    protected $name;
   /**
     * @ORM\Column(type="string" lenght=1000;)
     * @Assert\NotEmpty
     * @var string
     */
    protected $description;

    
    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }
    
    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }
}
?>