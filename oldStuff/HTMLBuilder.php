<?php
require_once(__DIR__."/config/dependenceLoader.php");
loadDependences(__FILE__);

class HTMLBuilder implements HTMLBuilder_IF {
    
    private $factory;
    
    public function __construct($factory) {
        $this->factory = $factory;
    }
    
    /*
    TODO:
    Rebuild website generation of website parts:
    - Head (Everything that website needs to display and function)
    - Navbar
    - Content (Content for specific part of the website)
    */
    
    public function createMainPage() {
        $page = $factory->createMainPage();
        return $page;
    }
    
    /*
    ei välttämttä tarvita?
    */
    public function createEventPage($json) {
        $page = new EventPage($json);
        return $page;
    }
    
    /*
    Returns html code for asked elements in a json format.
    Allows client to ask for html code for spesific elements form the server.
    Key is the element, and value is the type of the element.
    
    @json
    JSON:
    {
    head: "mainpage",
    body: "mainpage",
    navbar: "loggedInNavbar"
    }
    
    @return:
    JSON:
    {
    head: "<head>mainpage head</head>",
    body: "<body>mainpage body</body>",
    navbar: "<navbar>logged in navbar</navbar>"
    
    }
    
    Requires each element type as a php object.
    */
    public function getElements($json) {
        $contents = json_decode($json, true);
        
        foreach($contents as $key) {
            $contents[$key] = $factory->$contents[$key]();
        }
        
        return json_decode($contents);
    }

}


?>