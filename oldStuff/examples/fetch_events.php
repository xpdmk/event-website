<?php
require_once(__DIR__."/../bootstrap.php");
require_once(__DIR__."/classes/classes.php");

$eventRepository = $entityManager->getRepository('Event');
$events = $eventRepository->findAll();

foreach ($events as $event) {
    echo sprintf("-%s\n", $event->getName());
}
?>