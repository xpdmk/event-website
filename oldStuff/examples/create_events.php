<?php
// create_events.php
require_once "../bootstrap.php";
require_once "get_classes.php";

$newEventName = "Test event name";

$event = new Event();
$event->setName($newEventName);

$entityManager->persist($event);
$entityManager->flush();

echo "Created Product with ID " . $event->getId() . "\n";

?>