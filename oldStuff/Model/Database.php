<?php namespace AppBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;

class Database {
    
    private $entityManager;
    
    /**
     * Constructor, initializes the database and gets its classes.
     * @return void
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function getEntityManager() {
        return $this->entityManager;
    }
    
    
    # MYSQL METHODS
    # -------------
    
    /**
     * Opens a connection to the database.
     * @return void
     **/    
    private function openDB()
    {
        //Connect
        $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        //Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }
    
    /**
     * Prepares sql queries for selecting 10 most recent events and returns them.
     * @return String
     **/
    public function getNextEvents($limit = 10, $offset = 0)
    {
        
        return $this->getEntityManager()->getRepository("Event")->findBy(array(), array('id' => 'ASC'), $limit, $offset);
        /*//$ELCursor++;
        
        $qe      = "SELECT * FROM Event WHERE id >= (SELECT id FROM Event WHERE id = ".$this->ELCursor." ORDER BY id ASC LIMIT 1) LIMIT ".$amount.";";
        $qr      = "SELECT * FROM Review WHERE id >= (SELECT id FROM Event WHERE id = ".$this->ELCursor." ORDER BY id ASC LIMIT 1) LIMIT ".$amount.";";
        $qp      = "SELECT * FROM Presentation WHERE id >= (SELECT id FROM Event WHERE id = ".$this->ELCursor." ORDER BY id ASC LIMIT 1) LIMIT ".$amount.";";
        
        $this->ELCursor += $amount;
        
        return $this->get_eventListDB($qe,$qr,$qp);*/
    }
    
    /**
     * Prepares sql queries for selecting all events and returns them.
     * @return String
     **/
    public function getEventsOld() {
        // TODO: Remove function, replace with 
        
        $qe      = "SELECT * FROM Event;";
        $qr      = "SELECT * FROM Review;";
        $qp      = "SELECT * FROM Presentation;";
             
        return $this->get_eventListDB($qe,$qr,$qp);
    }
    
    /**
     * Operates a db search and returns the result as JSON string.
     * @qe,qr,qp : queries for event, review and presentation tables.
     * @return String
     **/
    public function get_eventListDB($qe,$qr,$qp)
    {
        $this->openDB();
        
        $jsonEventText = 
        $this->getDatabaseDataAsJSON($qe);
        $jsonReviewText = 
        $this->getDatabaseDataAsJSON($qr);
        $jsonPresentationText = 
        $this->getDatabaseDataAsJSON($qp);
        
        $jsonArray = array(
            "Event" => $jsonEventText,
            "Review" => $jsonReviewText,
            "Presentation" => $jsonPresentationText,);
        $jsonArray = json_encode(array('data'=>$jsonArray),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        
        $this->connection->close();
        return $jsonArray;
        exit;
    }

    /**
     * Prepares a sql query for selecting all event names and returns it.
     * @return String
     **/
    public function get_eventNames(){
        $q = "SELECT name FROM Event;";
        
        return $this->get_eventNamesDB($q);
    }
    
    /**
     * Runs a db search for getting all event names and returns the result as JSON string.
     * @q : query for event table
     * @return String
     **/
    public function get_eventNamesDB($q){
        $this->openDB();
        $jsonEventText = 
        $this->getDatabaseDataAsJSON($q);
        $this->connection->close();
        return $jsonEventText;
    }
    
    public function get_customer($email){
        $customer = $this->getEntityManager()
        ->getRepository("Customer")
        ->find($email);
        
        if(!$customer){
            throw $this->createNotFoundException(
            'No customer found for email '.$productId
            );
        }
        return $customer; 

    }

        
    /**
     * Runs the database query and returns the result as a JSON object.
     * @q : the query
     * @return object
     **/    
    public function getDatabaseDataAsJSON($q)
    {
        $result = mysqli_query($this->connection, $q);
        if ($result) {
            $jsonText = $this->convertSQLResultToJSON($result);
        } else {
            $jsonText = "Invalid query";
        }
        
        return $jsonText;
        
    }
    
    /**
     * Operates a doctrine search for getting an event by its ID.
     * Returns a JSON string. 
     * @id : id of the event we want to get.
     * @return string
     **/  
    // public function get_eventById($id) {
    //     $eventRepository = $this->getEntityManager()->getRepository("Event");
    //         $ids =  explode(",", $id);
    //         $eventArray = array();
    //         foreach($ids as $eventID) {
    //             $eventID = (int) $eventID;
    //             $event = $eventRepository->find($eventID);
    //             if (!is_object($event)) {
    //                 continue;
    //             }
    //             array_push($eventArray,$event);
       
    //         }
    //     return $this->convertObjectToJSONText($eventArray);
    // }
    
     public function getById($table, $id) {
        $object;
        if (gettype($table) != "string") {
            throw new RuntimeException("Invalid table name given, wasn't string: ".$table);
            exit;
        }
        
        // Check if id is int
        $id = (int) $id;
        
        $repository = $this->getEntityManager()->getRepository(ucfirst($table));
        $object = $repository->find($id);
        
        return $object;
    }
    
    public function getEvents() {
        
        $eventRepository = $this->getEntityManager()->getRepository("Event");
        $events = $eventRepository->findAll();
        return $events;
    }
    
    private function convertSQLResultToJSON($result) { // $result is MYSQL query request generated object
        // Array of dictionaries
        $dictionaries = array();
        foreach ($result as $row) {
        
            // Collect object field names (key) and field values (value) to a dictionary
            $dictionary = array();
            foreach ($row as $key => $value) {
                //
                if (is_numeric($value)) {
                    $value = (int) $value;
                } else if (gettype($value) == "string") {
                    $value = utf8_encode($value);
                }
                $dictionary[$key] = $value;
            }
            array_push($dictionaries, $dictionary);
        }
        //return dictionaries;
       $jsonText = json_encode($dictionaries, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        return $jsonText;
    }
    
      /**
         * Creates a path to images used in image carousel.
         * Returns a JSON string that has the paths to the images.
         * @eventId : the events id whomst'd've's images we are looking for.
         * @return string
         **/  
    public function get_carouselImages($eventId){
        $dir = 'img/'.$eventId.'/';
        $files = scandir($dir);
        $array = array();
        $index = 0;
        $allowed = array(".png", ".jpg");
        
    
        for($i = 0; $i<sizeof($files); $i++){
            
            if (in_array(substr($files[$i],-4), $allowed)) {
                $files[$i] = "/event-website/src/".$dir.$files[$i];
                $array[$index] = $files[$i];
                $index = $index + 1;
            }
        
        }
        $jsonText = json_encode($array, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        return $jsonText;
        
    }
    
    /**
     * Converts any object using object's get functions
     * 
     * Example:
     * Object has a private variable name that has been set to "Test".
     * The variable is returned with the object's public function getName.
     * Returns string {"name":"Test"}
     * 
     * @object : Object to be converted to JSON text
     **/ 
    public function convertObjectToJSONText($object) {
        $formating = JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES;
        $dictionary = $this->convertObjectToDictionary($object);
        $jsonText = json_encode($dictionary, $formating);
        return $jsonText; // addslashes();
    }
    
    	
	public function setTranslation($locale,$table,$attributes){
	    $table = ucfirst($table);
		$object = new $table();

		
		foreach ($attributes as $column => $value) {
		    
		    $column = ucfirst($column);
		    $function = 'set'.$column;
			$object->translate($locale)->$function($value);
		}
		
		$this->entityManager->persist($object);
		$object->mergeNewTranslations();
		$this->entityManager->flush();
		
	}

		
	public function getTranslation($locale,$object){
	    $translation = array();
	    
	    
	    if(get_class($object) == 'Event'){
	        print_r($object->translate($locale)->getDescription());
	        $description = $object->translate($locale)->getDescription();
	        
	        $translation['description'] = $description;
	    }
	    
	    $name = $object->translate($locale)->getName();
        $translation['name'] = $name;
       
	    
		return $translation;
	}
	
	
}