<?php namespace AppBundle\Model;

use AppBundle\Model\Converter;

/**
 * Converts dictionaries (key value pair arrays) to JSON text
 **/
class Textifier implements Converter {
    private $encoding;
    private $flags;
    
    public function Textifier($encoding, $flags = null) {
        $this->encoding = $encoding;
        $this->flags = $flags;
    }
    
    /**
     * Converts a dictionary to JSON text
     * @return : String
     */
    public function convert($dictionary) {
        $dictionary = $this->encode($dictionary);
        $jsonText = json_encode($dictionary, $this->flags);
        return mb_convert_encoding($jsonText, $this->encoding);
    }
    
    /**
     * Encodes all text to set encoding in the dictionary
     */
    private function encode($dictionary) { // TODO: Korjaa kysymysmerkit erikoismerkkien kohdalla -Roy
        $encodedDictionary = array();
        foreach (array_keys($dictionary) as $key) {
            // Encode key
            $encodedKey = $key;
            if (gettype($key) == "string") {
                $encodedKey = mb_convert_encoding($key, $this->encoding);
            } else if (is_numeric($key)) {
                $encodedKey = (int) $key;
            }
            
            // Encode value
            $value = $dictionary[$key];
            $encodedValue;
            if (gettype($value) == "array") {
                $encodedValue = $this->encode($value);
            } else if (is_numeric($value)) {
                $encodedValue = (int) $value;
            } else {
                $encodedValue = mb_convert_encoding($value, $this->encoding);
            }
            
            // Insert encoded key value pair to new dictionary
            $encodedDictionary[$encodedKey] = $encodedValue;
        }
        return $encodedDictionary;
    }
    
}