<?php namespace AppBundle\Model;

use AppBundle\Model\Converter;
use AppBundle\Interpreter\DateTimeInterpreter;
/**
 * Converts objects to dictionaries
 **/
class Dictionaryfier implements Converter {
    
    // TODO: Recode convert, convertUsingTags and handleArray functions to one
    
    public function __construct() {
        
    }
    
    /**
     * Converts given object to a key value array
     * @object : Object to be converted
     * @specs : Associative array that determines what instance variables to get. For example, for object Person get name and age with functions getName and getAge:
     * ["Person":["name","age"]]. Leave $variables null to get all variables
     * @lang : String key for the language. Supports only knplabs doctrine behaviors translatable objects. null = all languages
     * @return : Converted associative array of given object
     **/
    public function convert($object, $specs = null, $lang = null) {
        // Check type
        switch (gettype($object)) {
            case "array":
                return $this->processArray($object, [], $specs, $lang);
            case "object":
                return $this->processObject($object, [], $specs, $lang);
            default:
                return $object;
        }
    }
    
    /**
     * Converts any object to a key value array
     * @object : Object to be converted
     * @parents : Array of parents of the object
     * @specs : Array that determines what instance variables of the object to get
     * @return : Associative array
     **/
    private function processObject($object, $parents, $specs, $lang = null) {
        echo get_class($object)."\n";
        
        // Convert object to an array or just a string
        switch (get_class($object)) {
            case "DateTime":
                $array = array();
                if ($specs["DateTime"] === null) {
                    $array = array(
                        "year",
                        "month",
                        "day",
                        "hour",
                        "minute",
                        "timezone"
                    );
                    
                }
                $iterpreter = new DateTimeInterpreter($object);
                foreach ($array as $item) {
                    array_push($array, $iterpreter->$item);
                }
                return $array;
            case "Doctrine\Common\Collections\ArrayCollection":
                return $this->convertArray($object->toArray());
            default:
                // Get object variables and their get functions
                $vars = get_object_vars($object);
                print_r(((array) $object)[0]);
                $getFuncs = $this->createGetFunctions($vars);
                $varFuncCombination = array_combine($vars, $getFuncs);
                
                // Filter out functions that reference to parent objects
                $referencingFuncs = $this->getReferencingFunctions($object, $getFuncs, $parents);
                if (sizeof($referencingFuncs) > 0) {
                    // Remove functions that reference to parent objects
                    foreach($referencingFuncs as $func) {
                        unset($getFuncs[$func]);
                    }
                }
                
                // Convert object to a key value array
                $output = array();
                foreach ($varFuncCombination as $var => $function) {
                    $value = $this->processGetVariable($object, $function);
                    
                    $nextParents = array_merge([$object], $parents);
                    switch (gettype($value)) {
                        case "object":
                            $value = $this->processObject($value, $nextParents, $specs, $lang);
                            break;
                        case "array":
                            $value = $this->processArray($value, $nextParents, $specs, $lang);
                            break;
                    }
                    $output[$var] = $value;
                }
                
                // Add translated fields if translatable object
                if (in_array("Knp\DoctrineBehaviors\Model\Translatable\Translatable", class_uses(get_class($object)))) {
                    $translation = $object->translate($lang);
                    if (is_object($translation)) {
                        $translation = $this->processObject($translation, array_merge([$object], $parents), $specs, $lang);
                        echo gettype($translation);
                        $output = array_merge($output, $translation);
                    }
                }
        }
    }
    
    /**
     * Filters function names that reference to parent objects
     * @object : Object to test its functions
     * @parents : Array of parent objects of the object
     * @return : Array of functions that reference parent objects
     **/
    private function getReferencingFunctions($object, $getFuncs, $parents) {
        
        // Include only functions that do not reference to any parent object
        $array = array();
        foreach($getFuncs as $getFunc) {
            $variable = $this->processGetVariable($object, $getFunc);
            
            if (is_object($variable["value"])) {
                $notFound = true;
                foreach($parents as $parent) {
                    if ($variable["value"] == $parent) {
                        $notFound = false;
                        break;
                    }
                }
                if ($notFound) {
                    array_push($array, $getFunc);
                }
            } else {
                array_push($array, $getFunc);
            }
        }
        return $array;
    }
    
    /**
     * Converts array of objects/values to an array of key value array
     * @array : Array object that contains the objects/values
     * @parents : Array of parent objects of the array object
     * @return : Array of associative arrays of objects
     **/
    private function processArray($array, $parents, $specs, $lang = null) {
        $output = [];
        foreach ($array as $key => $value) {
            switch(gettype($value)) {
                case "array":
                    $output[$key] = $this->processArray($value, $parents, $specs, $lang);
                    break;
                case "object":
                    $nextParents = array_merge($parents, [$value]);
                    $output[$key] = $this->processObject($value, $nextParents, $specs, $lang);
                    break;
                default:
                    $output[$key] = $value;
            }
        }
        return $output;
    }
    /**
     * Takes object instance variable names and turn them into responding get functions
     * @variable : Array of object instance variable names
     * @return : Array of get functions for object instance variable names
     */
    private function createGetFunctions($variables) {
        $getFuncs = [];
        foreach ($variables as $variable) {
            array_push($getFuncs, "get".ucfirst($variable));
        }
        return $getFuncs;
    }
    
    /**
     * Uses object's function to get object's field name and value. Converts them to a key value array
     * @object : Object to be used
     * @function : String that contains the function name
     * @return : Associative array with "key" that has the variable name and "value" that has the value of the variable
     **/
    private function processGetVariable($object, $function) {
        $variable_name = lcfirst(substr($function, 3, strlen($function) - 3));
        $variable_value = $object->$function();
        
        $variable = array("key" => $variable_name,
            "value" => $variable_value);
        return $variable;
        
    }
    
    /**
     * Extracts strings that have "get" in the beginning
     * @functions : Array of string to be filtered
     * @return : Array of extracted function name strings with "get" as the 3 first letters
     **/
    private function getGetFunctions($functions) {
        $array = array();
        if (sizeof($functions) < 1) return $array;
        foreach($functions as $f) {
            if (substr($f, 0, 3) == "get") {
                array_push($array, $f);
            }
        }
        return $array;
    }
    
    /**
     * Converts object to dictionary (key value pair array) using an array of function names
     * @object : Object to be converted
     * @getFuncs : Array of strings of get functions for the object
     * @return : Created associative array of the object
     **/
    // private function process($object, $getFuncs, $lang = null) {
    //     $dictionary = array();
    //     foreach($getFuncs as $getFunc) {
    //         $variable = $this->processGetVariable($object, $getFunc, $lang);
            
    //         // Convert key to UTF-8
    //         //$variable["key"] = $this->convertValue($variable["key"]);
            
    //         // Check if value or object
    //         if (is_object($variable["value"])) {
    //             $result = $this->processObject($variable["value"], [$object], $lang);
    //             if ($result == null) {
    //                 continue;
    //             }
    //             $variable["value"] = $result;
    //         } 
    //         /*else {
    //             $variable["value"] = $this->convertValue($variable["value"]);
    //             if ($variable["value"] == null) {
    //                 continue;
    //             }
    //         }*/
    //         $dictionary[$variable["key"]] = $variable["value"];
    //     }
    //     return $dictionary;
    // }
}
interface Hasher_IF{
    public function createHash($password);

}
class Hasher implements Hasher_IF{
    private $options;
    
    public function __construct(){
         $this->options = [
        'cost'=>12,
        ];
    }
        
    public function createHash($password){
        $hash = password_hash($password, PASSWORD_BCRYPT, $this->options);
        return $hash;
    }
    
}
?>