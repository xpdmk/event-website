<?php

use PHPUnit\Framework\TestCase;

use AppBundle\Model\Dictionaryfier;

use AppBundle\Entity\Event;

final class DictionayfierTest extends TestCase {
    protected $dictionaryfier;
    
    public function setUp() {
        $this->dictionaryfier = new Dictionaryfier;
    }
    
    public function testObjectConvertionToDictionary() {
        $event = new Event;
        
        $name = "Shitty McShitshit";
        $event->translate("en")->setName($name);
        
        $dictionary = $this->dictionaryfier->convert($event, null, "en");
        
        $this->assertTrue(gettype($dictionary) == "array");
        $this->assertTrue($dictionary["name"] == $name);
    }
    
    public function testConvertObjectToDictionaryStringAndBack() {
        $event = new Event;
        
        $name = "Skabde ding dong";
        $event->translate("en")->setName($name);
        $dictionary = $this->dictionaryfier->convert($event, null, "en");
        $this->assertTrue(gettype($dictionary) == "array");
        $text = $this->textifier->convert($dictionary);
        $this->assertTrue(gettype($text) == "string");
        
        // Check if converts back
        $dictionary = json_decode($text, true);
        $this->assertTrue($dictionary["name"] == $name);
    }
    
    
}