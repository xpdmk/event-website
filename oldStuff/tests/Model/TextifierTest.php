<?php

use PHPUnit\Framework\TestCase;

use AppBundle\Model\Dictionaryfier;
use AppBundle\Model\Textifier;

use AppBundle\Entity\Event;

final class TextifierTest extends TestCase {
    
    protected $encoding;
    protected $dictionaryfier;
    protected $textifier;
    
    public function setUp() {
        $this->dictionaryfier = new Dictionaryfier;
        $this->textifier = new Textifier("UTF-8");
    }
    
    public function createTestArray() {
        $event = new Event;
        $event->translate("en")->setName("Shitty McShitshit");
        $dictionary = $this->dictionaryfier->convert($event, null, "en");
        $this->assertTrue(gettype($dictionary) == "array");
        return $dictionary;
    }
    
    public function testConvertArray() {
        $array = $this->createTestArray();
        
        // Convert array to string
        $string = $this->textifier->convert($array);
        
        // Check if converted successfully
        $this->assertTrue(gettype($array) == "string");
        
        // Convert back to array
        $array2 = json_encode($string, JSON_UNESCAPED_UNICODE);
        
        // Check if array converted correctly, i. e. it has same content
        $ŧhis->assertEmpty(array_diff($array, $array2));
    }
}