<?php
// Load dependences
require_once(__DIR__."/config/dependenceLoader.php");
loadDependences(__FILE__);

class MainPage extends Page {
    private $str;
    
    /**
     * Generates main page HTML code using array of events
     */
    public function __construct($json) {
        $properties = loadProperties("default", __FILE__);
    
        // Create HTML properties tags
        $beginning = new Template("html/documentProperties.tpl");
        $beginning->set("lang", "en"); // TODO: Toteuta haku jsonista
        
        // Add head data to head
        $head = new HTMLElement("head");
        $headContent = $properties;
        $head->setInnerHTML($headContent);
        
        $container = new HTMLElement("div");
        $container->setAttribute("class", "container");
        
        // Create list items
        $listItemsString = "";
        for ($i = 0; $i < sizeof($json); $i++) {
            $event = $json[$i];
            $listItem = $this->createListItem($event);
            $listItemsString .= $listItem;
        }
        
        // Add list item string to 
        $listGroup = new Template("html/listGroup.tpl");
        $listGroup->set("class", "list-group");
        $listGroup->set("id", "event-list");
        $listGroup->set("value", $listItemsString);
        
        // Create container
        $container = new HTMLElement("div");
        $container->setAttribute("class", "container");
        $container->setInnerHTML($listGroup);
        
        $navbar = new Template("html/navbar.tpl");
        
        # Construct body
        $body = new HTMLElement("body");
        $body->setInnerHTML($navbar.$container);
        // Finish HTML code
        $this->str = $beginning.$head.$body;
    }
    
    /**
     * @return : string of HTML code of main page
     */
    public function __toString() {
        return $this->str;
    }
    
    /**
     * Generates list item HTML code string from an event associative array
     * 
     * @eventJSON : Associative array of the event
     * @return : String of HTML code
     */
    private function createListItem($eventJSON) {
        $imgsrc = "img/".$eventJSON["id"].".png";
        
        $listItem = new Template("html/mainPageListItem.tpl");
        $listItem->set("title", $eventJSON["title"]);
        $listItem->set("description", $eventJSON["description"]);
        $listItem->set("imageSource", $imgsrc);
        
        $starsString = $this->createStars($eventJSON["reviews"]);
        $listItem->set("stars", $starsString);
        
        // TODO: 3 viimeisimmän tapahtuman tiedot näkyviin
        
        return $listItem->__toString();
    }
    
    /**
     * Generates star element HTML code string from event reviews
     * 
     * @reviews : array of Review objects translated to key pair arrays
     * $return : string of elements
     */
    private function createStars($reviews) {
        $starAmount = 5;
        
        // Prepair HTML elements
        $starElementFull = new HTMLElement("span");
        $starElementFull->setAttribute("aria-hidden", "true");
        $starElementEmpty = clone $starElementFull;
        $starElementFull->setAttribute("class", "glyphicon glyphicon-star");
        $starElementEmpty->setAttribute("class", "glyphicon glyphicon-star-empty");
        
        // Take average and cast to int
        $average = 0;
        for ($i = 0; $i < (sizeof($reviews)); $i++) {
            $average += $reviews[$i]["stars"];
        }
        $average = intval($average/(sizeof($reviews)));
        
        $starsString = "";
        // Add full stars
        for ($i = 0; $i < $average; $i++) {
            $starsString .= $starElementFull;
        }
        
        // Add empty stars
        for ($i = 0; $i < ($starAmount - $average); $i++) {
            $starsString .= $starElementEmpty;
        }
        
        return $starsString;
    }
}
?>