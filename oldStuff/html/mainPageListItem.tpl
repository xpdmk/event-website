<li class="list-group-item">
    <div class="row">
        <div class="col-xs-2">
            <img src="[@imageSource]" class="img-rounded img-listitem"></img>
        </div>
        <div class="col-xs-7">
            <div class="panel panel-default panel-listItem">
                <div class="panel-heading">
                    <a class="panelTitle" href="[@eventPageLink]">
                        [@title]
                    </a>
                </div>
                <div class="panel-body fixed-panel">
                    [@description]
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="ratings">
                <span class="label label-warning label-stars">
                [@stars]
                </span>
            </div>
            <ul class="list-group">
                [@presentationListItems]
            </ul>
        </div>
    </div>
</li>
