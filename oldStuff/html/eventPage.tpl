<div class="container">

    <!--Otsikko-->
    <div class="header">
        <div id="title" class="title">
            [@title]
        </div>
        <div id="rating" class="rating">
            [@rating]
        </div>
    </div>

    <!-- Media -->
    <div class="image-controller">
        [@carousel]
    </div>

    <!--Lisää tähän descriptioni-->
    <div id="description" class="well">
       [@description]
    </div>

    <!--Lisää näytökset-->
    <div class="presentatioins">
        <div class="table-responsive">
            <table id="presentations-table" class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="color-code-row">paikkoja</th>
                        <th>sijainti</th>
                        <th>kesto</th>
                        <th>päivämäärä</th>
                    </tr>
                </thead>
                <tbody id="presentation-body">
                    [@presentaion-body]
                    </tr>
                </tbody>

            </table>
        </div>
    </div>

</div>