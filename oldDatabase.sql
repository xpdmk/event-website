DROP DATABASE IF EXISTS event_db;

CREATE DATABASE test_db;

USE test_db;

CREATE TABLE Event
(
    EventID INT NOT NULL AUTO_INCREMENT,
    EventName VARCHAR(50),
    Description VARCHAR(1000),
    LinkToTrailer VARCHAR(50),
    PRIMARY KEY (EventID)
);

CREATE TABLE PostalCode
(
    PostalCode VARCHAR(10),
    City VARCHAR(20),
    PRIMARY KEY (PostalCode)
);

CREATE TABLE Location
(
    LocationID INT NOT NULL AUTO_INCREMENT,
    PostalCode VARCHAR(10),
    LocationName VARCHAR(50),
    StreetAddress VARCHAR(50),
    PRIMARY KEY (LocationID),
    FOREIGN KEY (PostalCode) REFERENCES PostalCode(PostalCode)
);

CREATE TABLE DiscountGroup
(
    DiscountGroupID INT NOT NULL AUTO_INCREMENT,
    DiscountGroupName VARCHAR(20),
    PRIMARY KEY (DiscountGroupID)
);

CREATE TABLE SeatType
(
    SeatTypeID INT NOT NULL AUTO_INCREMENT,
    SeatTypeName VARCHAR(50),
    PRIMARY KEY (SeatTypeID)
);

CREATE TABLE Customer
(
    CustomerID INT NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(15),
    LastName VARCHAR(15),
    Email VARCHAR(50),
    PRIMARY KEY (CustomerID)
);

CREATE TABLE Review
(
    ReviewID INT NOT NULL AUTO_INCREMENT,
    EventID INT NOT NULL,
    CustomerID INT NOT NULL,
    Stars TINYINT UNSIGNED,
    PRIMARY KEY (ReviewID),
    FOREIGN KEY (EventID) REFERENCES Event(EventID),
    FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
);

CREATE TABLE Space
(
    SpaceID INT NOT NULL AUTO_INCREMENT,
    LocationID INT,
    PRIMARY KEY (SpaceID),
    FOREIGN KEY (LocationID) REFERENCES Location(LocationID)
);

CREATE TABLE Seat
(
    SeatID INT NOT NULL AUTO_INCREMENT,
    SeatTypeID INT,
    SpaceID INT,
    SeatNumber INT,
    RowNumber INT,
    PRIMARY KEY (SeatID),
    FOREIGN KEY (SpaceID) REFERENCES Space(SpaceID),
    FOREIGN KEY (SeatTypeID) REFERENCES SeatType(SeatTypeID)
);

CREATE TABLE Organizer
(
    OrganizerID INT NOT NULL AUTO_INCREMENT,
    OrganizerName VARCHAR(50),
    PRIMARY KEY (OrganizerID)
);

CREATE TABLE Presentation
(
    PresentationID INT NOT NULL AUTO_INCREMENT,
    EventID INT,
    LocationID INT,
    StartDate DATE,
    EndDate DATE,
    StartTime TIME,
    EndTime TIME,
    OrganizerID INT,
    PRIMARY KEY (PresentationID),
    FOREIGN KEY (EventID) REFERENCES Event(EventID),
    FOREIGN KEY (LocationID) REFERENCES Location(LocationID),
    FOREIGN KEY (OrganizerID) REFERENCES Organizer(OrganizerID)
);

CREATE TABLE Price
(
    PriceID INT NOT NULL AUTO_INCREMENT,
    PresentationID INT,
    SeatTypeID INT,
    DiscountGroupID INT,
    Price DOUBLE,
    PRIMARY KEY (PriceID),
    FOREIGN KEY (PresentationID) REFERENCES Presentation(PresentationID),
    FOREIGN KEY (SeatTypeID) REFERENCES SeatType(SeatTypeID),
    FOREIGN KEY (DiscountGroupID) REFERENCES DiscountGroup(DiscountGroupID)
);

CREATE TABLE Ticket
(
    TicketID INT NOT NULL AUTO_INCREMENT,
    SeatID INT,
    PriceID INT,
    CustomerID INT,
    PRIMARY KEY (TicketID),
    FOREIGN KEY (SeatID) REFERENCES Seat(SeatID),
    FOREIGN KEY (PriceID) REFERENCES Price(PriceID),
    FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
);
INSERT INTO Event (EventName, Description, LinkToTrailer) VALUES ("Arrival",
    "Arrival on ohjaaja Denis Villeneuven (Sicario, Vangitut) ajatuksia herättävä science fiction -trilleri. Kun salaperäisiä avaruusaluksia laskeutuu ympäri maapalloa kielitieteilijä Louise Banksin (Amy Adams) johtama eliittitiimi kutsutaan tutkimaan. ",
    "https://youtu.be/3GSqOUTpfeI"
);
INSERT INTO Event (EventName, Description, LinkToTrailer) 
    VALUES ("La La Land", "Here is for the fools who dream.\nLa La Land -elokuva kertoo aloittelevan näyttelijän Mian (Emma Stone) ja omistautuneen jazz-muusikon Sebastianin (Ryan Gosling) tarinan. He ponnistelevat unelmiensa eteen kaupungissa, joka tunnetusti tuhoaa toiveet ja särkee sydämet. Nykyhetken Los Angelesin päivittäisestä elämästä kertova alkuperäismusikaali tutustuttaa unelmien tavoittelun iloihin ja suruihin.\nOscar-®ehdokas Damien Chazellen (Whiplash) käsikirjoittama ja ohjaama La La Land on rakkaudenosoitus enkelten kaupungille ja Hollywood-musikaaleille.", "https://www.youtube.com/watch?v=0pdqf4P9MB8");
INSERT INTO Event (EventName, Description, LinkToTrailer) VALUES("Assassin's Creed",
    "Assassin’s Creed perustuu suosittuun toimintapeliin, joka on saanut vahvasti vaikutteita oikeista historian tapahtumista, henkilöistä ja paikoista.\nMullistavan teknologian avulla Callum Lynch (Michael Fassbender) käy läpi esi-isänsä, Aguilarin (Michael Fassbander) kokemuksia inkvisition ajan Espanjassa 500 vuotta sitten. Callum saa kuulla kuuluvansa mystiseen salaseuraan, assassiineihin eli salamurhaajiin – ja hänen toisessa aikakaudessa keräämiään tietoja ja taitoja tarvitaan taistelussa mahtavaa ja tyrannimaista Templar-organisaatiota vastaan.",
    "https://www.youtube.com/watch?v=0fITmuqh4g8");
INSERT INTO Event (EventName, Description, LinkToTrailer) VALUES("Life, Animated",
    "Life, Animated kertoo autistisesta Owen Suskindista, joka oppi puhumaan Disney-animaatioiden kautta. Owenin vanhemmat kertovat oman puolensa pojan lapsuusajasta, ja hänen omat kokemuksensa tuolta ajalta on osin animoitu. Owen kertoo tietenkin myös itse elämästään ja kasvustaan autistisesta lapsesta itsenäiseksi mieheksi, joka uskoo tulevaisuuteen ja haluaa vain elää hyvä elämän, kuten me kaikki muutkin.",
    "https://www.youtube.com/watch?v=4n7fosK9UyY");
    
INSERT INTO Customer (FirstName, LastName, Email) VALUES ("Pate", "Esimerkki", "pate.esimerkki@gmail.com");

INSERT INTO Review (EventID, CustomerID, Stars) VALUES (1, 1, 2);
INSERT INTO Review (EventID, CustomerID, Stars) VALUES (2, 1, 5);
INSERT INTO Review (EventID, CustomerID, Stars) VALUES (3, 1, 3);
INSERT INTO Review (EventID, CustomerID, Stars) VALUES (4, 1, 4);
