# Event Website
Our vision is to create the one website to visit for upcoming events. Visitors can browse through millions of events and buy or book tickets to movies, concerts and fairs or register for free events. Event organizers can submit their own events to make selling tickets easy.
