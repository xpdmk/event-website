<?php

namespace AppBundle\Util;

use AppBundle\Model\Database;
use AppBundle\Model\Textifier;
use AppBundle\Model\Dictionaryfier;
use AppBundle\Model\Hasher;
use AppBundle\Entity\Customer;

interface DataManagerInterface{
    public function getById($table, $id);
    public function getEvents();
    public function getNextEvents($amount);
    public function getSearchResources();
    public function login($email,$password);
    
}

class DoctrineDataManager implements DataManagerInterface{
    private $databaseManager;
    private $dictionaryfier;
    private $textifier;
    
    public function __construct($entityManager) {
        $this->databaseManager = new Database($entityManager);
        $this->dictionaryfier = new Dictionaryfier();
        $this->textifier = new Textifier("UTF-8", JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }
    
    private function convertObjectToJSONText($object) {
        $dictionary = $this->dictionaryfier->convert($object);
        return $this->textifier->convert($dictionary);
    }
    
    public function insertCustomer($firstname,$lastname,$email,$password){
        
        $_em = $this->databaseManager->getEntityManager();
        $customer = new Customer();
        $emails = $_em->createQueryBuilder()
        ->select('c.email')
        ->from('AppBundle:Customer','c')
        ->getQuery()
        ->getResult();
        
        $emailExists = false;
        $email = strtolower($email);
        foreach($emails as $comparedEmail){
            $comparedEmailToLower = strtolower($comparedEmail["email"]);
            
            if($email == $comparedEmailToLower){
                $emailExists = true;
                return false;
                break;
            }
            
        }
        
        if (!$emailExists){
            $hasher = new Hasher();
            $customer = new Customer();
            
            $customer->setFirstName($firstname);
            $customer->setLastName($lastname);
            $customer->setEmail($email);
            //Hash the password with createHash that returns hash+salt
            $password = $hasher->createHash($password);
            $test =$customer->setPassword($password);
    
            $_em->persist($customer);
            $_em->flush();
            return true;
        }
    }
    
    public function deleteCustomer($email){
        $_em = $this->databaseManager->getEntityManager();
        $customer = $this->getCustomerByEmail($email);
        
        if (is_object($customer)) {
            $_em->remove($customer);
        } else {
            return false;
        }
        $_em->flush();
        return true;  
    }
    
    public function emailInUse($email) {
        $_em = $this->databaseManager->getEntityManager();
        $customer = $this->getCustomerByEmail($email);
        return ($customer != null)? true:false;
    }
     
    public function getCustomerByEmail($email){
        $_em = $this->databaseManager->getEntityManager();
        $customerRepository = $_em->getRepository("AppBundle:Customer");
        $customer = $customerRepository->findBy(array('email' => $email));
        
        if (sizeof($customer) < 1) {return false;}
        try {
            $customer = $customer[0];
        } catch (Exception $e) {
            return false;
        };
        return $customer;
    }
    
    public function login($email,$password){
        $customer = $this->getCustomerByEmail($email);
        if($customer!=null){
            $hash = $customer->getPassword();
            return password_verify($password,$hash);
        }
        return false;
    }

  
    public function getById($table, $id) {
        $object = $this->databaseManager->get($table, $id);
        return $this->dictionaryfier->convert($event);
    }
    
    public function getEventsOld() {
        return $this->databaseManager->getEvents();
    }
    
    public function getEvents() {
        return $this->convertObjectToJSONText($this->databaseManager->getEvents());
    }
    
    public function getNextEvents($amount) {
        $events = $this->databaseManager->getNextEvents($amount);
        return $this->dictionaryfier->convert($events);
    }
    
    /**
     * Operates a doctrine search for getting all event names and ID:s.
     * Returns a JSON string. 
     * @return string
     **/
    public function getSearchResources() {
        $events = $this->databaseManager->getEvents();
        
        $tags = array("id", "name");
        $resources = $this->dictionaryfier->convertUsingTags($events, $tags);
        
        return $this->textifier->convert($resources);
    }
}
?>