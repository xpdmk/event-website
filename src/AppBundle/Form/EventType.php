<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\PresentationType;
use AppBundle\Form\CategoryType;
use AppBundle\Form\EventTypeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\EventType as et;
use AppBundle\Entity\Category as c;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {	
    	$et = new et();
    	$c = new c();

   
    	
        $builder
        	->add('translations', TranslationsType::class, [
	    'fields' => [                               
	        'description' => [                       
	            'field_type' => TextareaType::class,               
	            'label' => $options['label']['description'],                
	            
	        ],
	        'name' => [                       
	            'field_type' => TextType::class,               
	            'label' => $options['label']['name'],                 
	            
	        ]
	    ],
	])
            ->add('linkToTrailer', TextType::class,array('label' => $options['label']['linktotrailer']))
            //->add('eventTypes',  CollectionType::class, array(
			//	'entry_type' => EventTypeType::class,
			//	'label' => $options['label']['eventtypes']
			//))
			  ->add('eventTypes', ChoiceType::class, array(
		    'choices' => array(
		        $options['label']['movie'] => $et,
		        $options['label']['theatre'] => $et),
				'label' => $options['label']['eventtypes']
			))
			->add('categories', ChoiceType::class, array(
		    'choices' => array(
		        $options['label']['drama'] => $c,
		        $options['label']['comedy'] => $c,
		    	$options['label']['musical'] => $c,
		        $options['label']['sci-fi'] => $c),
				'label' => $options['label']['categories']
			))
			->add('presentations', CollectionType::class, array(
            'entry_type' => PresentationType::class,
            'allow_add' => true,
            'allow_delete' =>true,
            
            'label' => ' ',
            
            'by_reference' => false
        	))
            ->add('save', SubmitType::class, array('label' => 'save', 'translation_domain' => 'messages'))
            ->getForm();
            
        ;
    }
}

?>