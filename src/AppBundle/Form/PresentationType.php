<?php
namespace AppBundle\Form;

use AppBundle\Entity\Presentation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\LocationType;



class PresentationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder
        ->add('start', DateTimeType::class,array('label' => 'startdate', 'translation_domain' => 'messages'))
        ->add('end', DateTimeType::class,array('label' => 'enddate', 'translation_domain' => 'messages'))
        ->add('location', LocationType::class,array('label' => ' ', 'translation_domain' => 'messages'));
        
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Presentation::class,
        ));
    }
}
?>