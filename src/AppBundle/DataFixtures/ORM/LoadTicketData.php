
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Ticket;

class LoadTicketData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ticket = new Ticket();

        $manager->persist($ticket);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
