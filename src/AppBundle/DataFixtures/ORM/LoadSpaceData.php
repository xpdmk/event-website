
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Space;

class LoadSpaceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $space = new Space();

        $manager->persist($space);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
