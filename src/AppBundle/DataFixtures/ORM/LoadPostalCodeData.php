
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\PostalCode;

class LoadPostalCodeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $postalCode = new PostalCode();

        $manager->persist($postalCode);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
