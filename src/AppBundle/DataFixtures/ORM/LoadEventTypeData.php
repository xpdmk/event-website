
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\EventType;

class LoadEventTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $eventType = new EventType();

        $manager->persist($eventType);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
