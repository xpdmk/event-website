
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Review;

class LoadReviewData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $review = new Review();

        $manager->persist($review);
        $manager->flush();
    }
    
    public function getOrder() {
        return 4;
    }
}

?>
