
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Presentation;

class LoadPresentationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $presentation = new Presentation();
        $presentation->setStart(new \DateTime('now'));
        $presentation->setEnd(new \DateTime('now'));
        
        
        $manager->persist($presentation);
        $manager->flush();
    }
    
    public function getOrder() {
        return 3;
    }
}

?>
