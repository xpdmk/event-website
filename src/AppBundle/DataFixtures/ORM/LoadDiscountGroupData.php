
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\DiscountGroup;

class LoadDiscountGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $discountGroup = new DiscountGroup();

        $manager->persist($discountGroup);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
