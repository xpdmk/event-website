<?php namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use AppBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setType('drama');
        $category1->translate('fi')->setName('Draama');
        $category1->translate('en')->setName('Drama');
        $manager->persist($category1);
        $category1->mergeNewTranslations();
        
        $category2 = new Category();
        $category2->setType('comedy');
        $category2->translate('fi')->setName('Komedia');
        $category2->translate('en')->setName('Comedy');
        $manager->persist($category2);
        $category2->mergeNewTranslations();
        
        $category3 = new Category();
        $category3->setType('romance');
        $category3->translate('fi')->setName('Romantiikka');
        $category3->translate('en')->setName('Romance');
        $manager->persist($category3);
        $category3->mergeNewTranslations();
        
        $category4 = new Category();
        $category4->setType('thriller');
        $category4->translate('fi')->setName('Jännitys');
        $category4->translate('en')->setName('Thriller');
        $manager->persist($category4);
        $category4->mergeNewTranslations();
        
        $category5 = new Category();
        $category5->setType('sci-fi');
        $category5->translate('fi')->setName('Sci-fi');
        $category5->translate('en')->setName('Sci-fi');
        $manager->persist($category5);
        $category5->mergeNewTranslations();
        
        $category6= new Category();
        $category6->setType('musical');
        $category6->translate('fi')->setName('Musikaali');
        $category6->translate('en')->setName('Musical');
        $manager->persist($category6);
        $category6->mergeNewTranslations();
        
        $this->addReference('drama', $category1);
        $this->addReference('comedy', $category2);
        $this->addReference('romance', $category3);
        $this->addReference('thriller', $category4);
        $this->addReference('sci-fi', $category5);
        $this->addReference('musical', $category6);
        
        $manager->flush();
        
    }
    
    public function getOrder() {
        return 1;
    }
}

?>
