<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="Presentation")
 **/
class Presentation {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer") 
     * @ORM\GeneratedValue 
     */
    protected $id;
    
    /** @ORM\Column(type="datetime") **/
    protected $start;
    
    /** @ORM\Column(type="datetime") **/
    protected $end;
    
    /**
     * Many Presentations have One Event.
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="presentations")
     * @ORM\JoinColumn(name="eventId", referencedColumnName="id")
     */
    protected $eventId;
    
    /**
     * Many Presentations have One Organizer.
     * @ORM\ManyToOne(targetEntity="Organizer")
     * @ORM\JoinColumn(name="organizerId", referencedColumnName="id")
     */
    protected $organizerId;
    
    /**
     * Many Presentations have One Location.
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="location", referencedColumnName="id")
     */
    protected $location;
    

    public function getId(){
        return $this->id;
    }
    
    public function getStart(){
        return $this->start;
    }
    public function setStart($start){
        $this->start = $start;
    }
    
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end){
        $this->end = $end;
    }
    
    public function getEventId() {
        return $this->eventId;
    }
    public function setEventId($eventId) {
        $this->eventId = $eventId;
    }

    public function getOrganizerId()
    {
        return $this->organizerId;
    }
    public function setOrganizerId($organizerId)
    {
        $this->organizerId = $organizerId;
    }

    public function getLocation()
    {
        return $this->location;
    }
    public function setLocation($location)
    {
        $this->location = $location;
    }
}
