<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Organizer")
 **/
class Organizer {

    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    
     /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $organizerName;

    public function getId()
    {
        return $this->id;
    }

    public function getOrganizerName()
    {
        return $this->organizerName;
    }
    public function setOrganizerName($organizerName)
    {
        $this->organizerName = $organizerName;
    }

}
