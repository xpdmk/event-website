<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="SeatType")
 */
class SeatType {
	
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $seatTypeName;
    
    public function getSeatTypeName()
    {
        return $this->seatTypeName;
    }
    public function setSeatTypeName($seatTypeName)
    {
        $this->seatTypeName = $seatTypeName;
    }

    public function getId()
    {
        return $this->id;
    }

}
