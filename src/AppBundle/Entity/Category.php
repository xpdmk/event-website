<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Category")
 */
class Category {
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id; // Tarpeellinen ManyToMany suhteiden säilyttämiseksi Event:in ja Category:n välillä
    
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * Many Categories have Many Events.
     * @ORM\ManyToMany(targetEntity="Event", inversedBy="categories")
     */
    protected $events;
    
    public function getEvents() {
        return $this->events;
    }
    public function setEvents($events) {
        $this->events = $events;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Category
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addCategory($this);
        }
        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }
}
