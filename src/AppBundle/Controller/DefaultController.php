<?php
 
namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Event;
use AppBundle\Entity\Presentation;
use AppBundle\Entity\Category;
use AppBundle\Form\EventType;
use Doctrine\Common\Collections\ArrayCollection;




use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
 
class DefaultController extends Controller
{

     public function indexAction(Request $request)
    {
        // Doctrine access: $this->getDoctrine()
        $formFactory = $this->get('fos_user.registration.form.factory');
        $form = $formFactory->createForm();
        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $session = $request->getSession();
        $lastUsernameKey = Security::LAST_USERNAME;
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);
        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;
        // replace this example code with whatever you need
        return $this->render('default/homepage.html.twig', [
            "locale" => $request->attributes->get("_locale"),
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken
        ]);
    }
    
    public function newEventFormAction(Request $request)
    {       
        
        // create a event
        $event = new Event;
        
       
        $locale = $request->attributes->get("_locale");
        // $this->get('translator')->addResource('xlf', 'labels.en.xlf', 'en');
        // $this->get('translator')->addResource('xlf', 'labels.fi.xlf', 'fi');
        $translator = $this->get('translator');
        
        $form = $this->createForm(EventType::class, $event,
        array('label' => array(
            'description' => $translator ->trans('description'),
            'name' => $translator->trans('name'),
            'linktotrailer' => $translator->trans('link_to_trailer'),
            'eventtypes' => $translator->trans('event_type'),
            'categories' => $translator->trans('category'),
            'presentations' => $translator->trans('presentation'),
            'theatre' => $translator->trans('theatre'),
            'movie' => $translator->trans('movie'),
            'save' => $translator->trans('save'),
            'drama' => $translator->trans('drama'),
            'musical' => $translator->trans('musical'),
            'sci-fi' => $translator->trans('sci-fi'),
            'comedy' => $translator->trans('comedy')),
            'required'=> array('doctrine' =>$this->getDoctrine())
            ));
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine();
            $repository->getRepository('AppBundle:Event');
            
            $originalPresentations = new ArrayCollection();
        
            // Create an ArrayCollection of the current Presentation objects in the database
            foreach ($event->getPresentations() as $presentation) {
                $originalPresentations->add($presentation);
            }
            
              $originalets = new ArrayCollection();
            foreach ($event->getEventTypes() as $et) {
                $originalets->add($et);
            }
            
            $editForm = $this->createForm(EventType::class, $event);
        
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
            
                // remove the relationship between the presentation and the Event
                foreach ($originalPresentations as $presentation) {
                   $location = ($presentation->getLocation());
                   
                   $em->persist($location);
                   $em->persist($presentation);
                    
                }
                
                 foreach ($originalets as $et) {
                   $em->persist($et);
                    
                }
                }
            
            
            
    $em->persist($event);
            $event->mergeNewTranslations();
            $em->flush();
            
        return $this->redirectToRoute('homepage');
    }
    
        return $this->render('default/eventForm.html.twig', array(
            'form' => $form->createView(),
            'locale' => $locale
        ));
    }
    
    
}