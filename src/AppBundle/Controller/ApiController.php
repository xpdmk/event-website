<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
	Symfony\Bundle\FrameworkBundle\Controller\Controller,
	Symfony\Component\HttpFoundation\Request,
	Symfony\Component\HttpFoundation\Response,
	Symfony\Component\HttpFoundation\JsonResponse,
	JMS\Serializer\SerializerBuilder as SBuilder,
	AppBundle\Handler\UserSerializer as UserSerializer,
	JMS\Serializer\Handler\HandlerRegistry as HandlerRegistry;

class ApiController extends Controller {
	/**
	 * @Route("/api/table/{table}")
	 */
	public function requestAction(Request $request, $table) {
		$isGet = $request->isMethod('GET');
		
		if ($isGet) {
			try {
				$data = (array) json_decode($request->query->get("data"));
				if ($data != []) {
					$data["table"] = $table;
					return $this->processData($data);
				}
			} catch (Exception $e) {}
			return new JsonResponse(["message"=>"Data value is invalid JSON. Result: ".($data == []?"false":$data)]);
		} else {
			return new Response("Please, do an AJAX GET request.");
		}
	}
	
	private function processData($data) {
		$keys = ["fields",
			"order",
			"offset",
			"limit",
			"table",
			"language"];
		foreach ($keys as $key) {
			${$key} = isset($data[$key])?$data[$key]:null;
			if (gettype(${$key}) == "object") {
				${$key} = (array) ${$key};
			}
		}
		try {
			$table = ucfirst($data["table"]);
		} catch (Exception $e) {
			return new JsonResponse(["message" => "For some reason table wasn't set. Sorry about that."]);
		}
		try {
			$repository = $this->getDoctrine()->getRepository('AppBundle:'.$table);
		} catch (Exception $e) {
			return new JsonResponse(["message" => "Invalid table name ".$table."."]);
		}
		
		$objects = $repository->findBy($fields, $order, $limit, $offset);
		
		$serializer = SBuilder::create()
			->addDefaultHandlers()
			->configureHandlers(
				function(HandlerRegistry $registry) {
					$registry->registerSubscribingHandler(new UserSerializer());
				}
			)
			->build();
		
		$serializedObjects = $serializer->serialize($objects, "json");
		
		return new Response($serializedObjects);
	}
	
	private function filterEventsWithCategories($events, $categories) {
		$filteredEvents = [];
		// Check if categories is set
		if (is_null($categories) || (is_array($categories) && sizeof($categories) < 1)) {
			// If event object doesn't have demanded categories, exclude event object
			foreach ($events as $event) {
				$qualified = true;
				foreach ($event->getCategories() as $category) {
					// Check if category not found in event
					if (!in_array($category->getType(), $categories)) {
						$qualified = false;
					}
				}
				if ($qualified) {
					array_push($filteredEvents, $event);
				}
			}
		}
		return $filteredEvents;
	}
	
	private function removeUnnecessaryValues($array) {
		array_filter($array, function($var){return !is_null($var);} );
		return $array;
	}
}