<?php namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class TemplateController extends Controller {
    
    public function loadAction(Request $request) {
        $path = $request->attributes->get("path");
        $templateFolder = $this->get('kernel')->getRootDir()."/Resources/views/";
        $file = $templateFolder.$path;
        if (strstr($file, "..") || !file_exists($file)) {
            return new JsonResponse(["message" => "Invalid template file path '".$file."'"]);
        } else {
            return new Response(file_get_contents($file));
        }
    }
    
    private function createEventListItem($event, $language) {
        // Create template on-the-fly
        $template = $twig->createTemplate("{% import 'macros/eventListItems.html.twig' as eventListItems %}{{ eventListItems.eventListItem(event, lang) }}");
        // Return html code using the template
        return $template->render(array("event" => $event,
            "lang" => $language));
    }
}