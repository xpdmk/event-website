function eventPage(json) {
    console.log(json);
    var eventTitle = json.name;
    var eventDesc = json.description;
    var presentations = json.presentations;
    for (var presentation in presentations) {
        var link = "https://tapahtumasivusto-xpdmk.c9users.io/event-website/event/" + json.id + "/" + presentations[presentation].id;
        var availableTickets = 0;
        var location = presentations[presentation].location;
        var startTime = presentations[presentation].startTime;
        var endTime = presentations[presentation].endTime;
        var duration = startTime.substring(0, startTime.length-3) + "-" + endTime.substring(0, startTime.length-3);
        
        var startDate = presentations[presentation].startDate.split("-");
        startDate = startDate[2]+"."+startDate[1]+"."+startDate[0];
        
        
        var endDate = presentations[presentation].endDate.split("-");
        endDate = endDate[2]+"."+endDate[1]+"."+endDate[0];
        var date;
        if(startDate == endDate){
            date = startDate;
        } else {
            date = startDate + "-" + endDate;
        }
        
        addPresentationRow(link, availableTickets, location, duration, date);
    }

    var stars = getReviewAvrg(json.reviews);
    var votes = json.reviews.length;


    var title = document.getElementById("title");
    var ratingsSpan = document.getElementById("rating");
    var description = document.getElementById("description");

    title.appendChild(document.createTextNode(eventTitle));
    description.appendChild(document.createTextNode(eventDesc));

    fillStars(stars);
    var votesSpan = document.createElement("span");
    votesSpan.className = "votes";
    votesSpan.id = "votes";
    votesSpan.appendChild(document.createTextNode(votes + " votes"));
    ratingsSpan.appendChild(votesSpan);

    useDB("get_carouselImages");




    function addPresentationRow(link, availableTickets, location, duration, date) {
        var listBody = document.getElementById("presentation-body");
        var tr = document.createElement("tr");
        var selectButton = document.createElement("a");
        selectButton.href = link;
        selectButton.appendChild(document.createTextNode("Valitse näytös"))

        listBody.appendChild(tr);
        tr.appendChild(createTd(selectButton));
        tr.appendChild(generateAvailability(availableTickets));
        tr.appendChild(createTd(document.createTextNode(location)));
        tr.appendChild(createTd(document.createTextNode(duration)));
        tr.appendChild(createTd(document.createTextNode(date)));


        function createTd(data) {
            var td = document.createElement("td");
            td.appendChild(data)
            return td;
        }

        function generateAvailability(availableTickets) {
            var td = createTd(document.createTextNode(availableTickets));
            var color;
            if (availableTickets < 10) {
                color = "danger";
            }
            else if (availableTickets < 50) {
                color = "warning";
            }
            else {
                color = "success";
            }
            td.className = color;
            return td;
        }

    }

    function fillStars(stars) {
        var ratings = document.getElementById("rating");
        for (var i = 0; i < stars; i++) {
            //### create rating system ####
            var star = document.createElement("span");
            star.className = "glyphicon glyphicon-star";
            star.setAttribute("aria-hidden", "true");
            ratings.appendChild(star);
        }

        for (var i = 0; i < 5 - stars; i++) {
            var emptyStar = document.createElement("span");
            emptyStar.className = "glyphicon glyphicon-star-empty";
            emptyStar.setAttribute("aria-hidden", "true");
            ratings.appendChild(emptyStar);
        }
    }





}
