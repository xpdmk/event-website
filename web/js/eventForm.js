console.log("loaded");
var $collectionHolder;

// setup an "add a presentation" link
var $addPresentationLink = $('<div class="paddingbuttongirl"><button class=" plusbutton mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored">  <i class="material-icons">add</i></button></div>');
var $newLinkLi = $('<li></li>').append($addPresentationLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of presentations
    $collectionHolder = $('ul.presentations');

    // add a delete link to all of the existing presentation form li elements
    /*$collectionHolder.find('li').each(function() {
        addPresentationFormDeleteLink($(this));
    });
    */
    
    // add the "add a presentation" anchor and li to the presentations ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addPresentationLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new presentation form (see next code block)
        addPresentationForm($collectionHolder, $newLinkLi);
    });
});

function addPresentationForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a presentation" link li
    var $newFormLi = $('<div class ="paddingboy"><li></li></div>').append(newForm);
    $newLinkLi.before($newFormLi);
    addPresentationFormDeleteLink($newFormLi);
}

function addPresentationFormDeleteLink($presentationFormLi) {
    var $removeFormA = $('<button class=" paddingbuttonboy mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab">  <i class="material-icons">remove</i></button>');
    $presentationFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the presentation form
        $presentationFormLi.remove();
    });
}