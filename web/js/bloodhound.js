/* global Bloodhound, $, searchResources */
function setupBloodhound(json) {
    var links = json;

    var events = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        local: links
    });

    events.initialize();

    $('#bloodhound .typeahead').typeahead(null, {
        displayKey: 'name',
        source: events.ttAdapter(),
        templates: {
            suggestion: function(data) {
                return "<a href=https://tapahtumasivusto-xpdmk.c9users.io/event-website/event/" + data.id + ">" + data.name + "<br></a>";
                /*
                return "<a href='#' onclick='dosomething()' id='Enter'>" + data.name + "</a>";
                */
            }
        }
    });
}

/*
$(document).keypress(function(event){
    if(event.keyCode == 13){
        $("#Enter").trigger('click');
    }
});

function dosomething(){
    alert('...');
}
*/
