//= require Builder.js
//= require AJAXRequester.js
//= require bloodhound.js
var searchResults;
var mainpageController = {
    /**
     * Fires when mainpage is loaded.
     *  useDB('get_10Newest') returns 10 newest events from the database,
     *  useDB('get_eventNames') returns all event names from the database.
     **/
    mainPageLoaded: function() {
        requester.useDB('get_10Newest');
        listitem.hideOnScroll();
        navbarController.getSearchResources();
    }
};

var eventpageController = {
    /**
     * Fires when eventpage is loaded.
     *  useDB("get_event") returns the event's information.
     * @param {Object} json
     **/
    eventPageLoaded: function() {
        requester.useDB('get_searchResources');
        navbarController.getSearchResources();
    }
};

var navbarController = {

    setSearchResources: function(json) {
        setupBloodhound(json);
    },
    getSearchResources: function() {
        requester.useDB('get_searchResources');
    }
};

function makeRequest(request) {
    requester.useDB(request);
}

function buildContainer(html) {
    contentUpdater.clearContainer();
    contentUpdater.insertCode(html);
}
