//An array consisting of all event names
var eventNames = null;
var json = null;
var searchResults;

function setResults(sr) {
    this.searchResults = sr;
}

var contentUpdater =  {
    
    clearContainer: function() {
        var container = this.getContainer();
        while (container.firstChild) {
        container.removeChild(container.firstChild);
        }
        
    },
    insertCode: function(html) {
        var container = this.getContainer();
        this.clearContainer();
        container.innerHTML = html;
        
    },
    
    getContainer: function(){
        return document.getElementById("container");
    },
    setContainer: function(){
        
    }
  
  };

function getResults() {
    return this.searchResults;
}
//var currentHints = [];

/**
 * Stores event names from JSON to eventNames Array
 * 
 * @param {Object} json
 **/
function storeEventNames(json) {

    eventNames = json;
    var name = [];
    var EN_len = eventNames.length;
    for (var i = 0; i < EN_len; i++) {
        name.push(eventNames[i].name);

    }

    eventNames = name;
    console.log(eventNames);
}

/**
 * Hides the navbar when scrolling down and shows it when scrolling up.
 * 
 * @return {Void}
 **/
function hideOnScroll() {
    window.addEventListener('scroll', function(e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop;
        var hideOn = 300;
        if (distanceY > hideOn) {

            document.getElementById("navbar").style.visibility = "hidden";
        }
        else {
            if (document.getElementById("navbar").style.visibility == "hidden") {
                document.getElementById("navbar").style.visibility = "visible";
            }
        }
    });
}
// function giveSearchHint(q){
//     var currentHints = [];

//     if (q != "") {
//     q = q.toLowerCase();
//     var len = q.length;
//     var EN_len = eventNames.length;

//     for(var i =0;i<EN_len;i++) {
//       var name = eventNames[i].name;
//          var nameSub = name.substring(0,len).toLowerCase();
//         if (nameSub.includes(q)) {
//             currentHints.push(name);

//         }
//     } console.log(currentHints);
// }
//  var searchHints = document.getElementById("searchHints");
//  var text;
//  if(currentHints.length>0){
//  document.getElementById("searchHints").visibility = "visible";

//  for(i=0; i<currentHints.length;i++){
//      text = document.createTextNode(currentHints[i]+"\n");
//  searchHints.appendChild(text);
//  }
// }
//
//}

/**
 * Creates a listElement from JSON gotten from the response
 * 
 * @param {Object} response
 **/
function JSONtoListElement(response) {
    for (var i = 0; i < Object.keys(response).length; i++) {
        var listItem = makeListitem(response[i]);
        var eventList = document.getElementById("event-list");
        eventList.appendChild(listItem);
    }
    
    // var event = response.data.Event;
    // var review = response.data.Review;
    // var presentation = response.data.Presentation;

    // event = JSON.parse(event);
    // review = JSON.parse(review);
    // presentation = JSON.parse(presentation);

    // //Create an JSON where Reviews and Presentations are part of their event's properties.          
    // for (var i = 0; i < Object.keys(event).length; i++) {
    //     var Review = [];
    //     var presentationArray = [];
    //     for (var j = 0; j < Object.keys(review).length; j++) {

    //         if (event[i].id == review[j].eventId) {
    //             Review.push(review[j]);
    //         }
    //         event[i].Review = Review
    //     }
    //     for (var k = 0; k < Object.keys(presentation).length; k++) {
    //         if (event[i].id == presentation[k].eventId) {
    //             if (presentation[k] != null) {
    //                 presentationArray.push(presentation[k]);
    //             }
    //         }
    //         event[i].Presentation = presentationArray;
    //     }

    //     var listItem = makeListitem(event[i]);
    //     var eventList = document.getElementById("event-list");
    //     eventList.appendChild(listItem);
    // }
}

/**
 * Fires when mainpage is loaded.
 *  useDB('get_10Newest') returns 10 newest events from the database,
 *  useDB('get_eventNames') returns all event names from the database.
 **/
function mainPageLoaded() {
    useDB('get_10Newest');
    useDB('get_searchResources');
    hideOnScroll();
}

/**
 * Fires when eventpage is loaded.
 *  useDB("get_event") returns the event's information.
 * @param {Object} json
 **/
function eventPageLoaded(json) {

    json = JSON.parse(json);
    this.json = json;
    console.log("Event page loaded");
    eventPage(this.json);
    useDB('get_searchResources');
}

/**
 * Fires when searching an event.
 *  useDB('get_searchResults') returns all the events that match the search query.
 **/
function searchEvent() {
    useDB("get_searchResults");
}

/**
 *  Sets the eventpage's elements' values.
 *  Gets json response  from the AJAX request as a parameter.
 *  @param {Object} response 
 **/
function JSONtoEventPage(response) {
    alert("JSON TO EVENT PAGE");
}

/**
 *  Handles all the actions that require operating the database in any way.
 *  @param {String} action An action we want to run.
 **/
function useDB(action) {
    switch (action) {
        case 'get_10Newest':
            getJSONData("10Events", "Starter.php", JSONtoListElement); //Starter.php
            break;
        case 'get_allEvents':
            getJSONData("get_allEvents", JSONtoListElement);
            break;
        case 'get_eventNames':
            getJSONData("get_eventNames", "Starter.php", storeEventNames);
            break;
        case 'get_event':
            getJSONData("event", "EventController.php", JSONtoEventPage);
            break;
        case 'get_searchResources':
            getJSONData("searchResources", "EventController.php", setSearchResources);
            break;
        case 'get_carouselImages':
            getJSONData("carouselImages", "EventController.php", imageCarousel);
            break;
        case 'get_searchResults':
            getJSONData("searchEvents", "EventController.php", JSONtoListElement); //Starter.php
            break;
        default:
            // code
    }
}

function setSearchResources(json) {
    setupBloodhound(json);
}

/**
 *  Sends and handles all AJAX requests.
 *  @param {String} action An action we want to commit.
 *  @param {Function} callback A callback function we want to start after the AJAX gets its response.
 *  @param {String} file The file we want to use in the URL.
 **/
function getJSONData(action, file, callback) {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        console.log(this.readyState + " " + this.status);
        if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            console.log(action + "'s result is " + typeof(response) + ":\n" + response);
            if (typeof(response) == "string") {
                response = JSON.parse(response);
                console.log(action + " JSON:\n");
                console.log(response);
            }
            callback(response);
        }
    };
    var url;
    if (action == "carouselImages") {
        url = file + "/" + action + "/" + this.json.id;
    }
    else if (action == "searchEvents") {
        var ids = "";
        if (searchResults != null) {
            for (var i = 0, len = searchResults.length; i < len; i++) {
                ids += searchResults[i].id + ",";
            }

            ids = ids.slice(0, -1);


        }

        console.log(file + "/" + action + "?q=" + ids);

        url = file + "/" + action + "?q=" + ids; //type a head ehdotusten idt;

    }
    else {
        url = file + "/" + action;
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

}

/**
 *  Creates an listitem from a json, the item is used in mainpage's container.
 *  @param {Object} json The JSON we got from ajax request.
 **/
function makeListitem(json) {
    var imgsrc = "img/" + json.id + ".png";
    var eventLink = "/event-website/event/" + json.id;
    var eventDesc = json.description;
    var eventTitle = json.name;
    
    var stars = getReviewAvrg(json.reviews);
    

    var listItem = document.createElement("li"); // create listitem
    listItem.className = "list-group-item";

    var row = document.createElement("div"); //create row which includes 3 columns
    row.className = "row";

    var leftCol = document.createElement("div") // Left Column which includes an image
    leftCol.className = "col-xs-2";

    var image = document.createElement("img");
    image.src = imgsrc;
    image.className = "img-rounded img-listitem";

    var midCol = document.createElement("div"); //Middle column which includes title and description
    midCol.className = "col-xs-7";

    var midPanel = document.createElement("div");
    midPanel.className = "panel panel-default panel-listItem";

    var panelHeading = document.createElement("div");
    panelHeading.className = "panel-heading";

    var panelTitle = document.createElement("h3");
    panelTitle.className = "panel-title";

    var title = document.createElement("a")
    title.href = eventLink;
    title.appendChild(document.createTextNode(eventTitle));

    var panelBody = document.createElement("div"); // create panel body
    panelBody.className = "panel-body fixed-panel";
    panelBody.appendChild(document.createTextNode(eventDesc));

    var rightCol = document.createElement("div"); //Right column which includes rating stars and next two presentations
    rightCol.className = "col-xs-3";

    var ratings = document.createElement("div");
    ratings.className = "ratings";

    var ratingsSpan = document.createElement("span");
    ratingsSpan.className = "label label-warning label-stars";

    var presentationList = document.createElement("ul");
    presentationList.className = "list-group";

    //LEFT\\
    leftCol.appendChild(image); //add image to leftCol
    row.appendChild(leftCol); //add left col to row

    //MIDDLE\\
    panelTitle.appendChild(title); //add title to heading
    panelHeading.appendChild(panelTitle); //add title to panel heading
    midPanel.appendChild(panelHeading); //add panel heading to middle panel
    midPanel.appendChild(panelBody); // add body to mid panel
    midCol.appendChild(midPanel); //add middle panel to midCol
    row.appendChild(midCol); //add middle col to row

    //RIGHT\\
    for (var i = 0; i < stars; i++) { //add stars to ratings span
        var star = document.createElement("span");
        star.className = "glyphicon glyphicon-star";
        star.setAttribute("aria-hidden", "true");
        ratingsSpan.appendChild(star);
    }

    for (var i = 0; i < 5 - stars; i++) {
        var emptyStar = document.createElement("span");
        emptyStar.className = "glyphicon glyphicon-star-empty";
        emptyStar.setAttribute("aria-hidden", "true");
        ratingsSpan.appendChild(emptyStar);
    }

    ratings.appendChild(ratingsSpan); //add ratings span to ratings
    rightCol.appendChild(ratings); //add ratings to right col
    
    
    // Add presentations
    if (json.presentations != undefined) {
        var presentations = json.presentations;
        for (var i = 0; i < 2; i++) {
            var presentation = presentations[i];
            if (presentation == undefined) {
                continue;
            }
            var presElement = document.createElement("li");
            presElement.className = "list-group-item";
            presElement.appendChild(document.createTextNode(presentation.startDate + " " + presentation.startTime));
            presentationList.appendChild(presElement);
        }
    }

    rightCol.appendChild(presentationList); //add presentation list to right col
    row.appendChild(rightCol); //add right col to row



    listItem.appendChild(row); //add row to listitem
    return listItem;
}

/**
 *  Calculates the average rating of a movie
 *  @param {Array} reviewArray An array that contains all the ratings
 **/
function getReviewAvrg(reviewArray) {
    var key;
    var sum = 0;
    var many = 0;
    var avrg = 0;
    for (key in reviewArray) {
        sum += reviewArray[key].stars;
        many++;
    }
    avrg = sum / many;
    return parseInt(avrg);
}

/**
 *  Parses the trailer's URL to an HTML tag.
 *  @param {String} url An url.
 **/
function parseTrailer(url) {
    var trailerHTML = '<iframe width="560" height="315" src="' + url + '" frameborder="0" allowfullscreen></iframe>'
    return trailerHTML;
}

/**
 *  Gets presentations time, date and location.
 *  @param {Object} json
 *  @param {Number} presentationId
 **/
function getPresentation(json, presentationId) {

    if (json.Presentation[presentationId] != null) {
        var startDate = json.Presentation[presentationId].startDate;
        var endDate = json.Presentation[presentationId].endDate;
        var location = json.Presentation[presentationId].locationId;
        var date = startDate + " - " + endDate;

        if (startDate == endDate) {
            date = startDate;
        }
        if (location == null) {
            location = "Unknown location";
        }
        var pres = location + ": " + date;
    }
    else {
        var pres = "No events found :(";
    }

    return pres;
}

/**
 *  Creates the rotating images.
 *  @param {Array} list A list of picture directories
 **/
function imageCarousel(list) {
    var slideNumber = 0;
    for (var item in list) {
        var li = document.createElement("li");
        li.setAttribute("data-target", "#myCarousel");
        li.setAttribute("data-slide-to", slideNumber);
        var ol = document.getElementById("image-indicators");
        ol.appendChild(li);

        var inner = document.getElementById("image-inner");
        var div = document.createElement("div");
        if (slideNumber == 0) {
            div.className = "item active";
            li.className = "active";
        }
        else {
            div.className = "item";
        }
        var img = document.createElement("img");
        img.src = list[item];
        img.alt = "kuva";

        inner.appendChild(div);
        div.appendChild(img);

        slideNumber++;
    }



}

/**
 *  Clears the main page container
 **/
function clearList() {
    var lista = document.getElementById("event-list");
    while (lista.firstChild) {
        lista.removeChild(lista.firstChild);

    }

}

function test() {
    {
        var ids = "";
        if (searchResults != null) {
            for (var i = 0, len = searchResults.length; i < len; i++) {
                ids += searchResults[i].id + ",";
            }
            if (ids.length > 1) {}
            ids[ids.length - 1].remove;
        }

        console.log("file" + "/" + "action" + "?q=" + ids);
    }
}
